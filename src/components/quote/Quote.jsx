import styled from "@emotion/styled";

const ResultDiv = styled.div`
  color: #fff;
  font-family: Arial, Helvetica, sans-serif;
  span {
    font-weight: bold;
  }
`;

const Info = styled.p`
  font-size: 18px;
  span {
    font-weight: bold;
  }
`;

const Price = styled.p`
  font-size: 30px;
  span {
    font-weight: bold;
  }
`;

const Quote = ({result}) => {
  if (Object.keys(result).length === 0) return null;
  return (
    <ResultDiv>
      <Price>The price is: <span>{result.PRICE}</span></Price>
      <Info>Highest price of the day: <span>{result.HIGHDAY}</span></Info>
      <Info>Lowest price of the day: <span>{result.LOWDAY}</span></Info>
      <Info>Variation of the last 24 hours: <span>{result.CHANGEPTC24HOUR}</span></Info>
      <Info>Last updateis: <span>{result.LASTUPDATE}</span></Info>
    </ResultDiv>
  );
}
 
export default Quote;