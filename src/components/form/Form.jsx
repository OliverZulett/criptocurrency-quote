import styled from "@emotion/styled";
import axios from "axios";
import { useEffect, useState } from "react";
import useCriptocurrency from "../../hooks/currency/useCriptocurrency";
import useCurrency from "../../hooks/currency/useCurrency";
import Error from "../error/Error";

const Button = styled.input`
  margin-top: 20px;
  font-weight: bold;
  font-size: 20px;
  padding: 10px;
  background-color: #66a2fe;
  border: none;
  width: 100%;
  border-radius: 10px;
  color: #fff;
  transition: background-color .3s ease;
  &:hover {
    background-color: #326ac0;
    cursor: pointer;
  }
`;

const Form = ({saveCurrency, saveCriptoCurrency}) => {
  const [criptoCurrencyList, saveCriptcurrencies] = useState([]);
  const [error, saveError] = useState(false);
  const CURRENCIES = [
    { code: 'USD', name: 'Dolar' },
    { code: 'MXN', name: 'Mexican currency' },
    { code: 'EUR', name: 'Euro' },
    { code: 'GBP', name: 'pound sterling' }
  ];
  const [currency, SelectCurrency] = useCurrency('choose a currency', '', CURRENCIES);
  const [criptoCurrency, SelectCiptocurrency] = useCriptocurrency('choose a criptocurrency', '', criptoCurrencyList);
  useEffect(() => {
    const consultAPI = async () => {
      const url = `https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD`;
      const result = await axios.get(url);
      saveCriptcurrencies(result.data.Data);
    };
    consultAPI();
  }, []);
  const quoteCurrency = event => {
    event.preventDefault();
    if (currency === '' || criptoCurrency === '') {
      saveError(true);
      return;
    }
    saveError(false);
    saveCurrency(currency);
    saveCriptoCurrency(criptoCurrency);
  }
  return (
    <form
      onSubmit={quoteCurrency}
    >
      {error ? <Error message='There is an error'/> :null}
      <SelectCurrency/>
      <SelectCiptocurrency/>
      <Button
        type="submit"
        value="Calculate"
      />
    </form>
  );
}
 
export default Form;