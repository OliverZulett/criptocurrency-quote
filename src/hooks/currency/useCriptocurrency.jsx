import styled from "@emotion/styled";
import { Fragment, useState } from "react";

const Label = styled.label`
  font-family: 'Bebas Neue', cursive;
  color: #fff;
  text-transform: uppercase;
  font-weight: bold;
  font-size: 2.4rem;
  margin-top: 2rem;
  display: block;
`;

const SelectTag = styled.select`
  width: 100%auto;
  display: block;
  padding: 1rem;
  --webkit-appearance: none;
  border-radius: 10px;
  border: none;
  font-size: 1.2rem;
`;

const useCriptocurrency = (label, initialState, options) => {
  const [state, updateState] = useState(initialState);
  const SelectCriptoCurrency = () => (
    <Fragment>
      <Label>{label}</Label>
      <SelectTag
        onChange={event => updateState(event.target.value)}
        value={state}
      >
        <option value="">-- Select --</option>
        {options.map(option => (
          <option key={option.CoinInfo.Id} value={option.CoinInfo.Name}>{option.CoinInfo.FullName}</option>
        ))}
      </SelectTag>
    </Fragment>
  )
  return [state, SelectCriptoCurrency, updateState];
}

export default useCriptocurrency;