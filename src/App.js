import styled from "@emotion/styled";
import axios from "axios";
import { useEffect, useState } from "react";
import image from './assets/img/cryptomonedas.png';
import Form from "./components/form/Form";
import Quote from "./components/quote/Quote";
import Spinner from "./components/spinner/Spinner";

const Container = styled.div`
  max-width: 900px;
  margin: 0 auto;
  @media (min-width:992px) {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    column-gap: 2rem;
  }
`;

const Image = styled.img`
  max-width: 100%;
  margin-top: 5rem;
`;

const Heading = styled.h1`
  font-family: 'Bebas Neue', cursive;
  color: #FFF;
  text-align:left;
  font-weight: 700;
  font-size: 50px;
  margin-bottom: 50px;
  margin-top: 80px;
  &::after {
    content: '';
    width: 100px;
    height: 6px;
    background-color: #66A2FE;
    display:block;
  }
`;

function App() {
  const [currency, saveCurrency] = useState('');
  const [criptoCurrency, saveCriptoCurrency] = useState('');
  const [result, saveResult] = useState({});
  const [spinner, loadSpinner] = useState(false);
  useEffect( () => {
    const quoteCriptoCurrency = async () => {
      if(currency === '') return;
      const url = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${criptoCurrency}&tsyms=${currency}`;
      const result = await axios.get(url);
      saveResult(result.data.DISPLAY[criptoCurrency][currency]);
      loadSpinner(true);
    };
    setTimeout(() => loadSpinner(false), 3000);
    quoteCriptoCurrency();
  }, [currency, criptoCurrency])
  return (
    <Container>
      <div>
        <Image
          src={image}
          alt="cripto image"
        />
      </div>
      <div>
        <Heading>instant cryptocurrency trading</Heading>
        <Form
          saveCurrency={saveCurrency}
          saveCriptoCurrency={saveCriptoCurrency}
        />
        {spinner ? <Spinner/> : <Quote result={result} />}
      </div>
    </Container>
  );
}

export default App;
